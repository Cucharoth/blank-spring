package com.ufro.prueba.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
@RequestMapping("/")
public class MiController {

    @GetMapping("/some")
    public ResponseEntity<?> getPasajeroById(@PathVariable int id) {
        String pasajero = null;
        if (pasajero != null) {
            return ResponseEntity.ok(pasajero);
        } else {
            String message = "No encontrado";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        }

    }
}
