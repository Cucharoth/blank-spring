package com.ufro.prueba.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class HelperService {

    public List<String> readFile(String filePath) {
        List<String> file = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(filePath));
            String line;
            while ((line = br.readLine()) != null) {
                file.add(line);
            }
            br.close();
        } catch (Exception e) {
            System.out.println("Path not found: " + e);
        }
        return file;
    }

    public void writeFile(String text, String nombre){
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(nombre))){
            writer.write(text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<List<String>> readCSV(String filePath) {
        List<List<String>> file = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(filePath));
            String line;
            br.readLine();
            while ((line = br.readLine()) != null) {
                String[] values = line.split(";");
                file.add(Arrays.asList(values));
            }
            br.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        return file;
    }

    public void soutFile(List<String> file) {
        for (String line : file) {
            System.out.println(line);
        }
    }

    public void soutCSV(List<List<String>> file){
        for (List<String> line : file) {
            System.out.println(line);
        }
    }
}
