package com.ufro.prueba.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MiDto {
    private int id;
    private String correo;
    private Date fechaVuelo;
}
